# db migrations

When we make any changes in the our existing table, for example if we add or delete any column, data
that is already in the database needs to be migrated to the modified structure.

## Why migration is necessary

To add a column to a table instead of dropping that entire table or dropping all our models and then
creating them again to reflect our new change, we can use migration to apply those changes to the database.

## Flask-migrate

Flask-migrate is an extenstion that helps in database migration.

## Getting started

* Step 1. To start with migration, we create a migration repository using below command:

```python3
flask db init
```

This adds a migration folder in our folder.

* Step 2. Now we perform below command

```python3
flask db migrate
```

It adds a new .py file to versions folder inside migrations folder. Every new migration has a unique revision identifier associated with it. It is reflected in the .py file name. This file has two functions upgrade() and downgrade(). upgrade contains all the commands for the changes in the table made in the current revision. downgrade() contains all the commands to undo the changes in the current revision.

* Step 3. Currently these changes are not reflected in the database. To apply the changes, we perform below command:

```python3
flask db upgrade
```

This command applies those changes to our database.

It is very helpful in collaborative work where with the access of migrations file, anyone can know what changes has been made to the database and can apply those changes to his/her database using below command:

```python3
flask db upgrade
```

### How to use

1. If the person is creating a new database, he needs to follow step 1,2 and 3.
2. If the person is making changes in existing database, he needs to follow step 2 and 3.
3. If the person wants to apply changes provided by other persons, he needs to follow step 3.
